import React,{Component} from 'react';
import './App.css';
import Film from './Film/Film'
import styles from './App.module.css';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FilterResults from 'react-filter-search';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import {ThemeProvider, createMuiTheme, StylesProvider} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import { InputAdornment } from '@material-ui/core';
import {green, purple} from '@material-ui/core/colors';

class App extends Component{
  state = {
    films : [],
    directors : [],
    showFilms : true,
    updateButtonShow : false,
    index : null,
    searchTerm : '',
    value : '',
    data : [],
    sort : '',
    title : '',
    director : '',
    producer : '',
    year : '',
    budget : '',
    intervalId : 0
  };

  componentDidMount(){
    fetch('http://localhost:4000/directors').then((response) => response.json()).then(
      (json) =>{
        this.setState({ directors: json });
        console.log(this.state.directors);
      },
      (error) =>{
        console.log('Nieudana pr�ba pobrania danych ' + error);
      }
    );
    fetch('http://localhost:4000/films').then((response) => response.json()).then(
      (json) =>{
        this.setState({ films: json });
        console.log(this.state.films);
      },
      (error) =>{
        console.log('Nieudana pr�ba pobrania danych ' + error);
      }
    );
  }
  handleChangeFilter = (event) => {
    const { value } = event.target;
    this.setState({ value });
  }
  deleteArticleHandler = (articleIndex) => {
    const films = this.state.films;
    axios.delete('http://localhost:4000/films/' + films[articleIndex].id).then((response) => {
      console.log(response);
    });
    films.splice(articleIndex,1);
    this.setState({ films: films});
  };
  updateBookHandler = (filmIndex) => {
    this.scrollToTop();
    const updateButtonVisible = this.state.updateButtonShow;
    this.setState({ updateButtonShow: true });
    const films = this.state.films;
    this.setState({ index: filmIndex });
    document.getElementById('title').value = films[filmIndex].title;
    document.getElementById('director').value = films[filmIndex].director;
    document.getElementById('producer').value = films[filmIndex].producer;
    document.getElementById('year').value = films[filmIndex].year;
    document.getElementById('budget').value = films[filmIndex].budget;
  };

  addFilmToState = () => {
    const title = document.getElementById('title').value;
    const director = document.getElementById('director').value;
    const producer = document.getElementById('producer').value;
    const year = document.getElementById('year').value;
    const budget = document.getElementById('budget').value;
    const id = Math.floor(new Date() / 1000);

    const film = {
      id : id,
      title : title,
      director : director,
      producer : producer,
      year : year,
      budget : budget
    };
    const films = [ ...this.state.films ];
    films.push(film);
    this.setState({ films: films });

    document.getElementById('title').value = '';
    document.getElementById('director').value = '';
    document.getElementById('producer').value = '';
    document.getElementById('year').value = '';
    document.getElementById('budget').value = '';
    axios.post('http://localhost:4000/films', film).then((response) => {
      console.log(response);
    });
    if(!this.checkIfDirectorExists(film.director)){
      const director = {
        id : id,
        name : film.director
      };
      const directors = [ ...this.state.directors ];
      directors.push(director);
      this.setState({ directors: directors });
      axios.post('http://localhost:4000/directors', directors).then((resposnse) =>{
        console.log(resposnse);
      });
    }
    else{
      console.log('Istnieje taki re�yser');
    }
  };

  scrollStep(){
    if(window.pageYOffset === 0){
      clearInterval(this.state.intervalId);
    }
    window.scroll(0, window.pageYOffset - this.compareDocumentPosition.scrollStepInPx);
  }
  scrollToTop(){
    let intervalId = setInterval(this.scrollStep.bind(this), '0');
    this.setState({ intervalId: intervalId });
  }
  checkIfDirectorExists = (directorName) => {
    const directors = this.state.directors;
    for(let i = 0; i < directors.length; i++){
      if(directors[i].name === directorName){
        return true;
      }
    }
    return false;
  };

  updateBookToState = () => {
    const index = this.state.index;
    const films = [ ...this.state.films ];
    const title = document.getElementById('title').value;
    const director = document.getElementById('director').value;
    const producer = document.getElementById('producer').value;
    const year = document.getElementById('year').value;
    const budget = document.getElementById('budget').value;

    const film = {
      id : films[index].id,
      title : title,
      director : director,
      producer : producer,
      year : year,
      budget : budget
    };
    films.splice(index, 1, film);
    this.setState({ films: films });
    axios.put('http://localhost:4000/books/' + films[index].id, film).then((response) => {
      console.log(response);
    });
    document.getElementById('title').value = '';
    document.getElementById('director').value = '';
    document.getElementById('producer').value = '';
    document.getElementById('year').value = '';
    document.getElementById('budget').value = '';
    this.setState({
      updateButtonShow : false
    });

  };
  toggleFilmsHandler = () => {
    const doesShow = this.state.showFilms;
    this.setState({ showFilms: !doesShow });
  };

  compareTitles(first, second){
    const titleA = first.title.toUpperCase();
    const titleB = second.title.toUpperCase();
    let comparison = 0;
    if(titleA > titleB){
      comparison = 1;
    }
    else if (titleA < titleB){
      comparison = -1;
    }
    return comparison;
  }
  compareDirectors(first, second){
    const directorA = first.director.toUpperCase();
    const directorB = second.director.toUpperCase();
    let comparison = 0;
    if(directorA > directorB){
      comparison = 1;
    }
    else if(directorA < directorB){
      comparison = -1;
    }
    return comparison;
  }
  compareBudgets(first, second){
    const budgetA = Number(first.budget);
    const budgetB = Number(second.budget);
    let comparison = 0;
    if(budgetA > budgetB){
      comparison = 1;
    }
    if(budgetA < budgetB){
      comparison = -1;
    }
    return comparison * -1;
  }

  setSort = (value) => {
    this.setState({ sort: value });
    const originalFilms = [ ...this.state.films ];
    let films = this.state.films;
    if(this.state.value === 0){
      this.setState({ films: originalFilms });
    }
    else if(value === 1){
      films.sort(this.compareBudgets);
      this.setState({ films: films });
    }
    else if(value === 2){
      films.sort(this.compareDirectors);
      this.setState({ films: films });
    }
    else if(value === 3){
      films.sort(this.compareTitles);
      this.setState({ films: films});
    }
  };

  handleChange = (event) => {
    this.setSort(event.target.value);
  };
  getDirectorName = (filmIndex) => {
    const film = this.state.films[filmIndex];
    const directorId = Number(film.directorId);
    const directors = this.state.directors;
    for(let i = 0; directors.length; i++){
      if(directors[i].id === directorId){
        return directors[i].name;
      }
    }
    return 'director.name';
  };

  getDirectorNameById = (directorId) => {
    const directors = this.state.directors;
    for(let i = 0; i < directors.length; i++){
      if(directors[i].id === directorId){
        return directors[i].name;
      }
    }
    return null;
  };

  findObjectByKey(array, key, value){
    for(var i = 0; i < array.length; i++){
      if(array[i][key] === value){
        return array[i];
      }
    }
    return null;
  }

  render() {
    const darkTheme = createMuiTheme({
      palette : {
        type : 'dark'
      }
    });
    const { data, value } = this.state;
    let films = null;
    const searchResult = (
      <FilterResults
        value = { value }
        data = { this.state.films }
        renderResults = {(results) => (
          <div>
            { results.map((el, index) => (
              <div>
                <Film
                  title = { el.title }
                  director = { el.director }
                  key = { el.id }
                  producer = { el.producer }
                  year = { el.year }
                  budget = { el.budget }
                />
              </div>
            ))}
          </div>
        )}
        />
    );
    let directors = this.state.directors;
    films = this.state.films.map((film, index) => {
      const directorName = JSON.parse(JSON.stringify(this.state.directors));
      const directorIndex = directorName.findIndex((x) => x.id === film.authorId)
      
      return(
        <Film
          title = { film.title }
          director = { film.director }
          deleteClick = {() => this.deleteArticleHandler(index)}
          updateClick = {() => this.updateFilmHandler(index)}
          key = { film.id }
          producer = { film.producer }
          year = { film.year }
          budget = { film.budget }
        />
      );
    });
    if(this.state.updateButtonShow){}
    const articleStyles = [];
    if(this.state.films.length === 1){
      articleStyles.push('One');
    }
    if(this.state.films.length >= 4){
      articleStyles.push('Green');
    }
    else{
      articleStyles.push('Orange');
    }
    const theme2 = createMuiTheme({
      palette : {
        primary : green
      }
    });
    const addButton = (
      <ThemeProvider theme = { theme2 }>
        <Button
          style = {{ margin: 10 }}
          id = "addButton"
          onClick = { this.addFilmToState }
          variant = "contained"
          color = "primary"
        >
          Dodaj Film
        </Button>
      </ThemeProvider>
    );
    const updateButton = (
      <Button
        style = {{ margin: 10 }}
        id = "addButton"
        onClick = { this.updateFilmToState }
        variant = "contained"
        color = "primary"
      >
        Aktualizuj film
      </Button>
    );

    const theme = createMuiTheme();
    return (
      <div className = { styles.App }>
        <ThemeProvider theme = { darkTheme }>
          <div className = { styles.form }>
            <div className = { styles.MyForm }>
              <div className = "container">
                <form autoComplete = "off">
                  <div>
                    <TextField
                      value = { this.state.title }
                      required
                      style = {{ margin: 8 }}
                      id = "title"
                      label = "Tytu�"
                      name = "title"
                      InputLabelProps = {{ shrink: true }}
                    />
                  </div>
                  <div>
                    <TextField
                      value = { this.state.director }
                      required
                      style = {{ margin: 8 }}
                      id = "director"
                      label = "Re�yser"
                      name = "director"
                      InputLabelProps = {{ shrink: true }}
                    />
                  </div>
                  <div>
                    <TextField
                      value = { this.state.producer }
                      required
                      style = {{ margin: 8 }}
                      id = "producer"
                      label = "Producent"
                      name = "producer"
                      InputLabelProps = {{ shrink: true }}
                    />
                  </div>
                  <div>
                    <TextField
                      value = { this.state.year }
                      required
                      style = {{ margin: 8 }}
                      id = "year"
                      label = "Rok wydania"
                      name = "year"
                      InputLabelProps = {{ shrink: true }}
                    />
                  </div>
                  <div>
                    <TextField
                      value = { this.state.budget }
                      required
                      style = {{ margin: 8 }}
                      id = "budget"
                      label = "Bud�et"
                      name = "budget"
                      InputLabelProps = {{ shrink: true }}
                    />
                  </div>
                </form>
              </div>
              { this.state.updateButtonShow ? updateButton : addButton }
              <br />
            </div>
          </div>
          <div className = { styles.mainCol }>
            <div className = { styles.sort }>
              <FormControl variant = "filled">
                <InputLabel id ="demo-simple-select-filled-label">Sortuj</InputLabel>
                <Select
                  labelId = "demo-simple-select-filled-label"
                  id = "demo-simple-select-filled"
                  value = { this.state.sort }
                  onChange = { this.handleChange }
                  style = {({ width: 14 + 'em'}, { minWidth: 200 + 'px'})}
                >
                  <MenuItem value = {0}><em>brak</em></MenuItem>
                  <MenuItem value = {1}>Cena: od najwy�szej</MenuItem>
                  <MenuItem value = {2}>Re�yser: od A do Z</MenuItem>
                  <MenuItem value = {3}>Tytu�: od A do Z</MenuItem>
                </Select>
              </FormControl>
            </div>
            <div>{ this.state.value ? searchResult : films }</div>
          </div>
          <div>
              <TextField
              value = { value }
              margin = "normal"
              label = "Filtruj"
              onChange = { this.handleChangeFilter }
              InputProps = {{
                startAdornment : (
                  <InputAdornment position = "start">
                    <SearchIcon />
                  </InputAdornment>
                )
              }}
              />

          </div>
        </ThemeProvider>
      </div>
    );
            }
}

export default App;
