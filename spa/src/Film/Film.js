import React from 'react';
import './Film.css';
import styles from './Film.module.css';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { createMuiTheme, withStyles, ThemeProvider } from '@material-ui/core/styles';
import { green, purple } from '@material-ui/core/colors';

const useStyles = makeStyles({
	card   : {
		minWidth : 275
	},
	bullet : {
		display   : 'inline-block',
		margin    : '0 2px',
		transform : 'scale(0.8)'
	},
	title  : {
		fontSize : 14
	},
	pos    : {
		marginBottom : 12
	}
});

const Film = (props) => {
	const classes = useStyles();
	const bull = <span className={classes.bullet}>•</span>;
	const theme = createMuiTheme({
		palette : {
			primary : green
		}
	});
	return (
		<div className={styles.article}>
			<Card className={classes.card} variant="outlined">
				<CardContent>
					<Typography variant="h5" component="h2">
						{props.title}
					</Typography>
					<Typography className={classes.pos} color="textSecondary">
						Reżyser: {props.director}
					</Typography>
					<Typography variant="body2" component="p">
						Rok wydania: {props.year}
					</Typography>
					<Typography variant="body2" component="p">
						Producent: {props.producer}
					</Typography>
					<Typography variant="body2" component="p">
						Budżet: {props.budget}
					</Typography>
				</CardContent>
				<CardActions>
					<Button
						variant="contained"
						color="primary"
						startIcon={<EditIcon />}
						size="small"
						onClick={props.updateClick}
					>
						Edytuj Film
					</Button>
					<Button
						variant="contained"
						color="secondary"
						startIcon={<DeleteIcon />}
						size="small"
						onClick={props.deleteClick}
					>
						Usuń Film
					</Button>
				</CardActions>
			</Card>
		</div>
	);
};

export default Film;
